#ifndef DELAY_H
#define DELAY_H

#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_tim.h"


extern void delay_us (uint16_t us);

#endif
