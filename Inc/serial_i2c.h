#ifndef SERIAL_I2C_H
#define SERIAL_I2C_H

#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"

#define SDA_ON			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_SET)
#define SDA_OFF			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_RESET)
#define SCL_ON			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_SET)
#define SCL_OFF			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_RESET)


void serial_i2c_start_condition(void);
void serial_i2c_stop_condtion(void);
void serial_i2c_transmit_addxx(uint8_t add, uint8_t r_w);
void serial_i2c_tx(uint8_t byte);
uint8_t serial_i2c_ack_nack_condition(void);


uint8_t serial_i2c_rx(void);
void serial_i2c_repeated_start_condition(void);

#endif
