#ifndef AT24C256_H
#define AT24C256_H

#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"

#define I2C_AT24_ADDRESS_1  0x50  /*A1 - GND, A0 - GND*/
#define I2C_AT24_ADDRESS_2  0x51  /*A1 - GND, A0 - VCC*/
#define I2C_AT24_ADDRESS_3  0x52  /*A1 - VCC, A0 - GND*/
#define I2C_AT24_ADDRESS_4  0x53  /*A1 - VCC, A0 - VCC*/

#define I2C_AT24_READ            0x01
#define I2C_AT24_WRITE           0x00

#define EEPROM_MAX_PAGE_BYTE_SIZE			0x40

#define EEPROM_START_PAGE_ADDRESS			0x0000
#define EEPROM_END_PAGE_ADDRESS				0x01FF

#define EEPROM_START_BYTE_ADDRESS			0x00
#define EEPROM_END_BYTE_ADDRESS				0x3F

#define EEPROM_EULER_START_PAGE_ADDRESS		0x0005

/*function to write data to eeprom at24c256*/
void AT24C256_byte_write(uint8_t byte_to_write);
void AT24C256_page_write(uint8_t rb_arr[],uint16_t rb_length);

/*function to read data from eeprom at24c256*/
void AT24C256_addrxx_read(uint16_t add, uint8_t rx_byte);
void AT24C256_sequential_read(uint16_t add, uint8_t rx_rb_arr[], uint16_t rx_length);

#endif
