#include "AT24C256.h"
#include "serial_i2c.h"
#include "delay.h"

static uint8_t multiple_page_write = 0;

/* A write operation requires two 8-bit data word addresses following the device
address word and acknowledgment. Upon receipt of this address, the EEPROM will again
respond with a zero and then clock in the first 8-bit data word. Following receipt of the 8-bit
data word, the EEPROM will output a zero. */

void AT24C256_byte_write(uint8_t byte_to_write)
{
	serial_i2c_start_condition();
	serial_i2c_transmit_addxx(I2C_AT24_ADDRESS_1, I2C_AT24_WRITE);
	serial_i2c_ack_nack_condition();

	uint16_t eeprom_address = ( (EEPROM_START_PAGE_ADDRESS << 6) | (EEPROM_START_BYTE_ADDRESS) );
	uint8_t msb_eeprom_address = ( (eeprom_address & 0xFF00) >> 8 );
	uint8_t lbs_eeprom_address = (eeprom_address & 0x00FF);

	serial_i2c_tx(msb_eeprom_address);
	serial_i2c_ack_nack_condition();
	serial_i2c_tx(lbs_eeprom_address);
	serial_i2c_ack_nack_condition();

	serial_i2c_tx(byte_to_write);
	serial_i2c_ack_nack_condition();

	serial_i2c_stop_condtion();
}

void AT24C256_page_write(uint8_t rb_arr[],uint16_t rb_length)
{
    /*Determining if the write is a multiply page write or a single page write*/
    if(rb_length > EEPROM_MAX_PAGE_BYTE_SIZE)
    {
        multiple_page_write = 1;
    }
    else
    {
        multiple_page_write = 0;
    }


    if(multiple_page_write == 1)
    {
    	/*MULTIPLE PAGE MULPTIPLE BYTE WRITE*/

        uint16_t no_of_pages = ( (rb_length / 64) + 1);

        /*setting start page for config file, odo and error data*/
        uint16_t eeprom_euler_start_page_address = EEPROM_EULER_START_PAGE_ADDRESS;

        for(int page = 0;page<no_of_pages;page++)
        {
            /*determine the last page for remaining bytes*/
            uint8_t length_of_page = 0;
            if(page == (no_of_pages - 1) )
            {length_of_page = (rb_length % EEPROM_MAX_PAGE_BYTE_SIZE);}
            else
            {length_of_page = EEPROM_MAX_PAGE_BYTE_SIZE;}

            /*differentiate between first page address write and start condition, and the next additive page address write
            without start condition this condition will be usefull with other eepoms if they allow rollover pages but AT24C256
            doesn't have that feature*/
            if(page == 0)
            {
            	serial_i2c_start_condition();
                serial_i2c_transmit_addxx(I2C_AT24_ADDRESS_1, I2C_AT24_WRITE);
                serial_i2c_ack_nack_condition();

                uint16_t eeprom_address = ( (eeprom_euler_start_page_address << 6) | (EEPROM_START_BYTE_ADDRESS) );
                uint8_t msb_eeprom_address = ( (eeprom_address & 0xFF00) >> 8 );
                uint8_t lbs_eeprom_address = (eeprom_address & 0x00FF);

                serial_i2c_tx(msb_eeprom_address);
                serial_i2c_ack_nack_condition();
                serial_i2c_tx(lbs_eeprom_address);
                serial_i2c_ack_nack_condition();
            }
            else
            {
            	serial_i2c_start_condition();
                serial_i2c_transmit_addxx(I2C_AT24_ADDRESS_1, I2C_AT24_WRITE);
                serial_i2c_ack_nack_condition();

                uint16_t eeprom_address = ( (eeprom_euler_start_page_address << 6) | (EEPROM_START_BYTE_ADDRESS) );
                uint8_t msb_eeprom_address = ( (eeprom_address & 0xFF00) >> 8 );
                uint8_t lbs_eeprom_address = (eeprom_address & 0x00FF);

                serial_i2c_tx(msb_eeprom_address);
                serial_i2c_ack_nack_condition();
                serial_i2c_tx(lbs_eeprom_address);
                serial_i2c_ack_nack_condition();
            }

            for(int byte = (page*64);byte<((page*64)+length_of_page);byte++)
            {
                serial_i2c_tx(rb_arr[byte]);
                serial_i2c_ack_nack_condition();
            }
            eeprom_euler_start_page_address+=1;
            serial_i2c_stop_condtion();
            delay_us(5);		/* delay for internal write sequence of eeprom*/
        }
    }
    else
    {
    	/*SINGLE PAGE MULPTIPLE BYTE WRITE*/
    	serial_i2c_start_condition();
        serial_i2c_transmit_addxx(I2C_AT24_ADDRESS_1, I2C_AT24_WRITE);
        serial_i2c_ack_nack_condition();

        uint16_t eeprom_address = ( (EEPROM_EULER_START_PAGE_ADDRESS << 6) | (EEPROM_START_BYTE_ADDRESS) );
        uint8_t msb_eeprom_address = ( (eeprom_address & 0xFF00) >> 8 );
        uint8_t lbs_eeprom_address = (eeprom_address & 0x00FF);

        serial_i2c_tx(msb_eeprom_address);
        serial_i2c_ack_nack_condition();
        serial_i2c_tx(lbs_eeprom_address);
        serial_i2c_ack_nack_condition();

        for(int i=0;i<rb_length;i++)
        {
            serial_i2c_tx(rb_arr[i]);
            serial_i2c_ack_nack_condition();
        }
        serial_i2c_stop_condtion();
        delay_us(5);	/* delay for internal write sequence of eeprom*/

    }
}

void AT24C256_addrxx_read(uint16_t add, uint8_t rx_byte)
{

}

void AT24C256_sequential_read(uint16_t add, uint8_t rx_rb_arr[], uint16_t rx_length)
{

}
