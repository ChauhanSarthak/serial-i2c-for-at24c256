/*
 * serial_i2c.c
 *
 *  Created on: 26-11-2022
 *  Author: Sarthak
 */

#include "serial_i2c.h"
#include "delay.h"



void serial_i2c_start_condition(void)
{
	SDA_ON;
	delay_us(1);
	SCL_ON;
	delay_us(4);
	SDA_OFF;
	delay_us(4);
	SCL_OFF;
	delay_us(4);
}

void serial_i2c_stop_condtion(void)
{
	SDA_OFF;
	delay_us(1);
	SCL_ON;
	delay_us(4);
	SDA_ON;
}

void serial_i2c_transmit_addxx(uint8_t add, uint8_t r_w)
{
	uint8_t address = 0x00;
	address = add << 1;
    address |= (r_w & 0x01);
    serial_i2c_tx(address);

}

uint8_t serial_i2c_ack_nack_condition(void)
{
	uint8_t status = 0;
	SDA_ON;
	delay_us(1);
	SCL_ON;
	/*read GPIO status of SDA line
	 * if sda_read == 1, NACK , status = 1
	 * else sda_read == 0, ACK , status = 0
	 */
	delay_us(4);
	SCL_OFF;
	return status;
}

void serial_i2c_tx(uint8_t byte)
{
	for(uint8_t i = 8; i>0; i--)
	{
		(byte & 0x80) ? SDA_ON : SDA_OFF;
		byte <<= 1;
		delay_us(1);
		SCL_ON;
		delay_us(4);
		SCL_OFF;
		delay_us(4);
	}
}

uint8_t serial_i2c_rx(void)
{
	uint8_t status = 0;
	return status;
}

void serial_i2c_repeated_start_condition(void)
{

}

